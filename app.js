window.onload = function() {
    
    init();
  //  somethingElse();
    
};

function init(){
    
    var head = document.getElementById('hd');
    var msg = document.getElementById('message');
    var err = document.getElementById('error');
    
    var backgroundReady = 'false';
    var messageReady = 'false';
    
    
    var promise1 = function(){
        
         var p1 = new Promise(function(resolve, reject){
        
             function setBackground(){
                 
                 document.body.style.backgroundColor = "green";
                 backgroundReady = 'true';
                 resolve('Changing background');
                 console.log('Changing background');
                 
             }
        
             setTimeout(setBackground, 8000);

        
    });
        
    return p1;    
        
    };
    
    
    var promise2 = function(){
        
         var p2 = new Promise(function(resolve, reject){
        
             function checkBackground(){
                 
                 if(backgroundReady == 'true'){
                     
                     msg.style.display = 'block';
                     
                     messageReady = 'true';
                     resolve('Display message');
                     console.log('display message');                     
                     
                 }
                 else{
                     
                     err.style.display = 'block';
                     reject('Background not set');
                     console.log('promise 2 rejected');
                     
                     
                 }
             }
             setTimeout(checkBackground, 5000);
                 
             });
        
             return p2;
        
    };
    
    var promise3 = function(){
        
         var p3 = new Promise(function(resolve, reject){
        
             function checkAll(){
                 
                 if(backgroundReady == 'true' && messageReady == 'true'){
                     
                     head.style.display = 'block';
                     resolve("Everything worked like a charm!")
                 }
                 else{
                     err.style.display = 'block';
                     reject('Failure!');
                  }
             }
             
             setTimeout(checkAll, 3000);
                 
             });
        
             return p3;
        
    };
    
    
    promise1().then(promise2, console.log('Promise 1 Pending'))
              .then(promise3, console.log('Promise 2 Pending, Promise 3 Pending'));
    

    
//    promise1().then(function(result){
//       console.log('promise 1: ', result);
//    });
//    
//    promise2().then(function(result){
//       console.log('promise 2: ', result);
//    })
//    .catch(function(reason){
//        console.log('Rejected becoz >> ' + reason);
//        
//    });
//    
//    promise3().then(function(result){
//       console.log('promise 3: ', result);
//    })    
//    .catch(function(reason){
//        console.log('Rejected becoz >> ' + reason);
//        
//    });
    
    
    
//    var myPromise = new Promise(function(resolve, reject){
//        
//        
//        setTimeout(function(){
//            resolve("Background changed");  //what we resolve here is passed onto the next part of the promise.
//           }, 1000);
//        
//    });
//    
//    myPromise.then(function(result){
//        console.log(result)
//        document.body.style.backgroundColor = "black";
//        
//    })
//    .catch(function(reason){
//        console.log('Rejected becoz' + reason);
//        
//    });
    
}

//function somethingElse(){
//    console.log('Something else executed');    
//}